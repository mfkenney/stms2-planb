#!/bin/bash
#
# Control the motors and trigger signal for an STMS2 backscatter run
#
t_bold="\033[1m"
t_unbold="\033[0m"

: ${ARBGEN_HOST=10.13.0.64}
: ${SCPI_PORT=5025}
: ${XDIST=2}

trigger_on ()
{
	echo 'outp1 on' | nc $ARBGEN_HOST $SCPI_PORT
}

trigger_off ()
{
	echo 'outp1 off' | nc $ARBGEN_HOST $SCPI_PORT
}

seq="$1"
if [ -z "$seq" ]
then
    echo "$(basename $0) starting_seq" 1>&2
    exit 1
fi

x=$XDIST
while true
do
	if (( x < 0 ))
	then
		dir="-X"
	else
		dir="+X"
	fi
	response=
	echo -n -e "Press ENTER to start seq${seq} ${t_bold}${dir}${t_unbold} run: "
	read -n1 response
	[ -z "$response" ] || exit 1
	seqdir="seq${seq}_raw"
	mkdir -p $seqdir
	trigger_on
	motor-client.py --dist $x --rec $seqdir/mcsdata
	trigger_off
	(( seq++ ))
	x=$(( x * -1 ))
done
