#!/bin/bash
#
# Drive the STMS2 tower on a full rail run in the +X and -X directions.
#
t_bold="\033[1m"
t_unbold="\033[0m"

: ${FULLRAIL=42}
: ${ARBGEN_HOST=10.13.0.64}

seq="$1"
if [ -z "$seq" ]
then
    echo "$(basename $0) starting_seq" 1>&2
    exit 1
fi

response=
echo -n -e "Press ENTER to start seq${seq} ${t_bold}+X${t_unbold} run: "
read -n1 response
[ -z "$response" ] || exit 1
seqdir="seq${seq}_raw"
mkdir -p $seqdir
# Enable Arbgen output (trigger signal)
echo 'outp1 on' | nc $ARBGEN_HOST 5025
motor-client.py --delay 8 --dist $FULLRAIL --rec $seqdir/mcsdata
# Disable Arbgen output
echo 'outp1 off' | nc $ARBGEN_HOST 5025
(( seq++ ))

echo -n -e "Press ENTER to start seq${seq} ${t_bold}-X${t_unbold} run: "
read -n1 response
[ -z "$response" ] || exit 1
seqdir="seq${seq}_raw"
mkdir -p $seqdir
# Enable Arbgen output (trigger signal)
echo 'outp1 on' | nc $ARBGEN_HOST 5025
motor-client.py --delay 8 --dist -${FULLRAIL} --rec $seqdir/mcsdata
# Disable Arbgen output
echo 'outp1 off' | nc $ARBGEN_HOST 5025