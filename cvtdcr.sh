#!/bin/bash
#
# Manage the STMS2 "Plan B" data conversion process.
#
if [[ $# -lt 1 ]]; then
    echo "`basename $0` seqid" 1>&2
    exit 1
fi

seq="$1"
seqdir=${1}_raw

cd $seqdir || exit 1

filelist="/tmp/dcrfiles.$$"
> $filelist
trap "rm -f $filelist;exit 0" 0 1 2 3 15

# Rename the raw data files to ping-N.dcr
i=0
for f in $(find rawdata -name 'seq*.dcr' -print | sort); do
    newfile="rawdata/ping-${i}.dcr"
    mv -v $f $newfile
    echo "$newfile" >> $filelist
    (( i++ ))
done
npings=$i

if [[ "$npings" -eq 0 ]]; then
    echo "No pings found!" 1>&2
    exit 1
fi

child=
if [[ -f mcsdata ]]; then
    if [[ ! -f mcs.nc ]]; then
        echo "Starting conversion of MCS data"
        mcs2netcdf.py mcsdata mcs.nc &
        child=$!
    fi
fi

mkdir -p cycle-0
# Activate the Python virtual environment
source $HOME/.venvs/planb/bin/activate
# Start processing the pings in parallel
ppss -f $filelist -c 'python -m dash32.data $ITEM cycle-0/$(basename $ITEM .dcr).nc'
# Compress the raw data files
tar -c -z -f rawdata.tgz rawdata
rm -rf rawdata
rm -rf ppss_dir
rm -f *.py *.pyc
if [[ -n "$child" ]]; then
    echo -n "Waiting for MCS conversion to finish ..."
    wait $child
    echo " done"
fi
cd ..
mv $seqdir $seq