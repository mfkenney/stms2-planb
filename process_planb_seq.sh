#!/bin/bash
#
# Monitor a Redis queue for each new sequence ID and convert the
# DCR files to netCDF.
#

dir="$1"
[ -n "$dir" ] && cd "$dir"

while true
do
    msg=$(redis-cli brpop "stms2:sequence" 0)
    seqid=$(sed -n -e 2p <<< "$msg")
    [ -n "$seqid" ] && cvtdcr.sh $seqid
done
