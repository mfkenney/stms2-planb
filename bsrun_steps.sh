#!/bin/bash
#
# Control the motors and trigger signal for an STMS2 backscatter run
#
t_bold="\033[1m"
t_unbold="\033[0m"

: ${ARBGEN_HOST=10.13.0.111}
: ${SCPI_PORT=5025}
: ${XSTEPS=40}

XDIST="0.1"
trigger ()
{
    echo 'trig' | nc $ARBGEN_HOST $SCPI_PORT
    #echo 'Trigger'
}

seq="$1"
if [ -z "$seq" ]
then
    echo "$(basename $0) starting_seq" 1>&2
    exit 1
fi

# Named pipes to interact with motor-ctl.py
p_in="/var/tmp/in_pipe"
p_out="/var/tmp/out_pipe"


x=$XDIST
while true
do
    if (( (seq%2) == 0 ))
    then
        dir="+X"
    else
        dir="-X"
    fi
    response=
    echo -n -e "Press ENTER to start seq${seq} ${t_bold}${dir}${t_unbold} run: "
    read -n1 response
    [ -z "$response" ] || exit 1
    seqdir="seq${seq}_raw"
    mkdir -p $seqdir
    rm -f $p_in $p_out
    mkfifo $p_in $p_out
    motor-ctl.py --rec $seqdir/mcsdata < $p_in 1> $p_out &
    bgpid=$!
    # Setup our input/output file descriptors
    exec 3>$p_in
    exec 4<$p_out
    for step in $(seq $XSTEPS)
    do
        # Wait for the single character prompt
        read -t 30 -n 1 <&4
        if [ $? -ne 0 ]
        then
            echo "No prompt from motor-ctl.py"
            kill "$bgpid"
            exit 1
        fi
        for t in $(seq 10)
        do
            trigger
            sleep 0.5
        done
        echo "move $x" >&3
    done
    echo "quit" >&3
    wait "$bgpid"
    (( seq++ ))
    x=$(echo "$x * 0.1" | bc -l)
done
