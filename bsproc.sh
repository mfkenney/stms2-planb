#!/bin/bash
#
# Process an STMS2 backscatter run.
#


if [ $# -lt 3 ]
then
    echo "Usage: $(basename $0) cfgfile rundir seqdir [ppc]"
    exit 1
fi

cfgfile="$1"
rundir="$2"
seqdir="$3"
ppc="$4"

[ -n "$ppc" ] || ppc=0

mkdir -p ${seqdir}/cycle-0
child=
if [ -f $seqdir/mcsdata ]
then
    if [ ! -f $seqdir/mcs.nc ]
    then
        echo "Starting conversion of MCS data"
        mcs2netcdf.py $seqdir/mcsdata $seqdir/mcs.nc &
        child=$!
    fi
fi

# Activate the Python virtual environment
source $HOME/.venvs/planb/bin/activate

ping=0
for f in $(ls ${rundir}/*.mat)
do
    if ((ppc > 0))
    then
        cycle=$((ping / ppc))
        idx=$((ping % ppc))
    else
        cycle=0
        idx="$ping"
    fi
    mkdir -p $seqdir/cycle-${cycle}
    echo -n "Processing ping $ping ... "
    bs2netcdf.py $cfgfile $f ${seqdir}/cycle-${cycle}/ping-${idx}.nc
    echo "done"
    (( ping++ ))
done

if [ -n "$child" ]
then
    echo -n "Waiting for MCS conversion to finish ..."
    wait $child
    echo " done"
fi
