#!/bin/bash
#
# Convert the mid-frequency backscatter data to netcdf
#
DATADIR="/data/TREX/STMS2"
RAWDIR="/data/TREX/STMS2_BS"

while read r s freq
do
    seqdir="$DATADIR/seq${s}_raw"
    run=$(printf "R%06d" $r)
    rundir=$(eval 'echo $RAWDIR/TREX13_${run}*')
    bsproc.sh $RAWDIR/f33_${freq}.yaml $rundir $seqdir
    mv -v $seqdir "$DATADIR/seq${s}"
done
