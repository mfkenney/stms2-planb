# STMS2 Plan B Checklist

## Before starting a +X run

1. Set Base Filename on DASH32 (Capture->Capture Settings).

2. Arm the Data Capture process on the DASH32.

3. If necessary, start *load_quad_gen4* and load the flat-top 6ms pulses into
   each channel.

4. Enable VLA amplifiers.

5. Select "Output Waveforms with External Trigger" from *load_quad_gen4*.

6. Start the trigger output from the Arbgen.

7. Start the +X motor move from *zeus*.


## At end of +X run

1. Stop the Arbgen trigger.

2. Stop the Data Capture process on the DASH32.


## Before starting a -X run

1. Change Base Filename on DASH32.

2. Arm the Data Capture process on the DASH32.

3. If *load_quad_gen4* has timed-out, reselect "Output Waveforms with External Trigger".

4. Start the trigger output from the Arbgen.

5. Start the -X motor move from *zeus*.


## At end of -X run

1. Stop Arbgen trigger.

2. Inhibit VLA amplifiers.

3. Stop the Data Capture process on the DASH32.

4. Start the Data Archiving process on the DASH32 (Configuration->Review).

5. Reformat the Capture Drive after archiving is complete (Capture->Capture Settings).

6. Copy DASH32 data to *zeus* after the Data Archiving has completed.


## At the end of the day

1. Delete the contents of the Archive Folder on the DASH32.
