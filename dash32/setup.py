#!/usr/bin/env python
from distutils.core import setup


setup(name="Pydash32",
      version=open('version.txt', 'r').read().strip(),
      description="DASH32 utility software",
      author="Michael Kenney",
      author_email="mikek@apl.uw.edu",
      url="http://wavelet.apl.uw.edu/~mike/python/",
      packages=["dash32"],
      scripts=["bin/splitdcr.py"])
