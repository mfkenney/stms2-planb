#!/usr/bin/env python
"""
%prog [options] chunksize dcrfile

Split a DASH32 DCR file into smaller chunks. The `chunksize` parameter
is the maximum number of samples (per channel) in each new file. The
new files have the same base-name as the original file with `_N` appended
where `N` is the chunk number.
"""
from dash32 import dcr
import os
from optparse import OptionParser
import time


def timestamp(secs, ticks):
    t = float(secs) + ticks * 20e-9
    return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(t))


def split_file(infile, size):
    buf = infile.read(size)
    while buf:
        yield buf
        buf = infile.read(size)


def split_dcr(hdr, infile, chunksize, basename):
    window = hdr.pre_trigger_periods + hdr.post_trigger_periods
    chunks, extra = divmod(window, chunksize)
    period = float(hdr.rate_divider) / float(hdr.base_sample_rate)
    # Length of each chunk in seconds
    interval = chunksize * period
    # Convert to integer seconds and 20-nanosecond ticks
    secs = int(interval)
    ticks = int((interval - secs) * 1e9 / 20.)
    channels = hdr.waveforms
    if hdr.events:
        channels += 1
    # Update header fields
    hdr.pre_trigger_periods = 0
    hdr.post_trigger_periods = chunksize
    # Size of each chunk in bytes
    block_size = chunksize * channels * 2
    for i, block in enumerate(split_file(infile, block_size), start=1):
        filename = '{0}_{1:04d}.dcr'.format(basename, i)
        if len(block) < block_size:
            hdr.post_trigger_periods = extra
        # Increment the timestamp
        hdr.time += secs
        hdr.time_ext += ticks
        print 'Chunk {0:d} time={1}'.format(i,
                                            timestamp(hdr.time, hdr.time_ext))
        with open(filename, 'wb') as ofile:
            dcr.header.build_stream(hdr, ofile)
            ofile.write(block)


def main():
    parser = OptionParser(usage=__doc__)
    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')
    infile = open(args[1], 'rb')
    chunksize = int(args[0])
    basename, ext = os.path.splitext(args[1])
    hdr = dcr.header.parse_stream(infile)
    split_dcr(hdr, infile, chunksize, basename)


if __name__ == '__main__':
    main()
