#!/usr/bin/env python
"""
.. module:: dcr
    :synopsis: define Dash32 DCR data structures.
"""
from construct import *


amplifier_structure = Struct(
    'amplifier_structure',
    LFloat64('span'),
    LFloat64('bottom_val'),
    LFloat64('max_span'),
    LFloat64('high_alarm'),
    LFloat64('low_alarm'),
    LFloat64('user_scale'),
    LFloat64('user_offset'),
    SLInt16('input1'),
    Padding(2),
    SLInt8('user_units_on'),
    Padding(3),
    SLInt8('high_alarm_on'),
    Padding(3),
    SLInt8('low_alarm_on'),
    Padding(3),
    String('units', 17),
    Padding(3),
    String('label', 33),
    Padding(3),
    SLInt32('module_id'),
    SLInt32('atten_setting'),
    SLInt8('filter_on'),
    Padding(3),
    Enum(SLInt32('filter_topology'),
         NULL=0,
         Bessel=1,
         Butterworth=2,
         Chebyshev=3
         ),
    Enum(SLInt32('filter_type'),
         Low_Pass=0,
         High_Pass=1,
         Band_Pass=2,
         Band_Reject=3
         ),
    Enum(SLInt32('filter_style'),
         IIR=0,
         FIR=1
         ),
    SLInt32('filter_order'),
    LFloat64('filter_cutoff1'),
    LFloat64('filter_cutoff2'),
    LFloat64('filter_ripple'),
    Enum(SLInt32('input_filter_type'),
         No_filter=0,
         Low_Pass=1,
         High_Pass=2,
         Band_Pass=3,
         Band_Reject=4,
         Custom=5,
         Rms=6
         ),
    Enum(SLInt32('input_filter_topology'),
         NULL=0,
         Bessel=1,
         Butterworth=2,
         Chebyshev=3
         ),
    LFloat64('input_filter_cutoff1'),
    LFloat64('input_filter_cutoff2'),
    LFloat64('input_filter_ripple'),
    LFloat64('rms_scale'),
    LFloat64('rms_offset'),
    SLInt32('user_units_precision'),
    SLInt32('nbits'),
    SLInt32('precision'),
    String('custom_filter_name', 128),
    String('rms_filter_name', 128),
    Padding(256)
)

# Don't care about this information
event_structure = StaticField('event_structure', 52)
view_structure = StaticField('view_structure', 6504)


header = Struct(
    'header_section',
    String('file_id', 8),
    SLInt32('version'),
    SLInt32('size'),
    SLInt32('amps_offset'),
    SLInt32('amps_size'),
    SLInt32('view_offset'),
    SLInt32('view_size'),
    SLInt32('waveforms'),
    SLInt32('events'),
    Padding(4),
    SLInt32('base_sample_rate'),
    SLInt32('rate_divider'),
    SLInt64('channels_stored'),
    SLInt64('pre_trigger_periods'),
    SLInt64('post_trigger_periods'),
    String('rec_name', 64),
    SLInt32('time'),
    SLInt32('time_ext'),
    SLInt64('pre_trigger_count'),
    SLInt64('post_trigger_count'),
    SLInt32('triggered'),
    SLInt32('data_type'),
    String('log_filename', 64),
    SLInt32('irig_ev_channel'),
    Padding(1024),
    Array(32, amplifier_structure),
    Array(8, event_structure),
    view_structure
)
