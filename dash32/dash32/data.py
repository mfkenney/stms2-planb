#!/usr/bin/env python
"""
.. module:: data
    :synopsis: read Dash32 DCR data files.
"""
import dcr
import numpy as np
from netCDF4 import Dataset
from collections import namedtuple


Receiver = namedtuple('Receiver', 'channel name gain')

#: Maximum span for the DASH32 A/D in counts
DASH32_MAX_SPAN = 60000


def read_dash32_dcr(infile):
    hdr = dcr.header.parse_stream(infile)
    cols = hdr.waveforms
    if hdr.events > 0:
        cols += 1
    data = np.fromfile(infile, np.int16)
    n = data.shape[0]
    rows = n // cols
    return hdr, np.reshape(data, (rows, cols))


def write_stms2_nc(filename, hdr, data, receivers):
    """
    Write an STMS2 compatible netCDF file from a Dash32
    data-set.
    """
    nc = Dataset(filename, 'w', format='NETCDF3_CLASSIC')
    nc.createDimension('rindex', hdr.waveforms)
    nc.createDimension('tau', data.shape[0])
    rnum = []
    gains = []
    namelen = 0
    for receiver in receivers:
        mask = 1 << receiver.channel
        if (mask & hdr.channels_stored) == mask:
            namelen = max(namelen, len(receiver.name))
            rnum.append(receiver.channel)
            gains.append(receiver.gain)
    nc.createDimension('namelen', namelen)
    setattr(nc, 'transmitter', 'null')
    setattr(nc, 'pulsefile', 'null')
    setattr(nc, 'dcr_name', hdr.rec_name.replace('\x00', ''))
    t_secs = nc.createVariable('time', 'i4', ())
    t_secs.units = 'seconds since 1/1/1970 00:00:00+00:00'
    t_secs.long_name = 'trigger time'
    t_usecs = nc.createVariable('usecs', 'i4', ())
    t_usecs.units = 'microseconds'
    period = nc.createVariable('period', 'f4', ())
    period.units = 'seconds'
    vpb = nc.createVariable('vpb', 'f4', ())
    vpb.units = 'volts'
    tvolts = nc.createVariable('tvolts', 'f4', ())
    rnums = nc.createVariable('rnum', 'i4', ('rindex', ))
    rnums.long_name = 'receiver a/d channel'
    rnames = nc.createVariable('rname', 'c', ('rindex', 'namelen'))
    rnames.long_name = 'STMS2 receiver name'
    gain = nc.createVariable('gain', 'i4', ('rindex', ))
    gain.units = 'dB'
    gain.long_name = 'programmable gain'
    preamp = nc.createVariable('preamp', 'i4', ('rindex', ))
    preamp.units = 'dB'
    preamp.long_name = 'fixed gain'
    rdata = nc.createVariable('rdata', 'i2', ('tau', 'rindex'))
    rdata.long_name = 'received A/D data'
    rdata.units = 'counts'
    t_secs.assignValue(hdr.time)
    t_usecs.assignValue(hdr.time_ext * 20 // 1000)
    period.assignValue(float(hdr.rate_divider) / float(hdr.base_sample_rate))
    vpb.assignValue(hdr.amplifier_structure[0].span / DASH32_MAX_SPAN)
    tvolts.assignValue(0.)
    rnums[:] = rnum
    # space-pad the string variables out to maxlen
    fmt = '%%-%ds' % namelen
    rnames[:] = [fmt % r.name for r in receivers]
    preamp[:] = np.zeros(hdr.waveforms, np.int32)
    gain[:] = gains
    rdata[:] = data
    nc.close()


if __name__ == '__main__':
    import sys
    receivers = []
    for i in range(3):
        receivers.append(Receiver(name='SAS-LF:LG{0:d}'.format(i + 1),
                                  channel=i,
                                  gain=0))
    for i in range(3, 6):
        receivers.append(Receiver(name='SAS-LF:HG{0:d}'.format(i + 1),
                                  channel=i,
                                  gain=20))
    f = open(sys.argv[1], 'rb')
    hdr, data = read_dash32_dcr(f)
    write_stms2_nc(sys.argv[2], hdr, data, receivers)
