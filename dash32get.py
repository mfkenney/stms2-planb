#!/usr/bin/env python
"""
dash32get.py [options] seqid [seqid ...]

Download STMS2 sequence data from the DASH32.
"""
import subprocess
from optparse import OptionParser
import os
import sys
import redis


# DATA_PATH = 'Data Capture Files/Dash 32HF Sample'
DATA_PATH = 'data capture files'


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(host='10.13.0.138', user='dash32hf',
                        password='dash32hf', subdir='rawdata')
    parser.add_option('--host',
                      type='string',
                      dest='host',
                      help='Dash32 hostname or IP address')
    parser.add_option('-s', '--subdir',
                      type='string',
                      dest='subdir',
                      help='name of subdirectory for DCR data files')
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('Missing sequence ID')

    for seqid in args:
        url = 'ftp://{0}/{1}/{2}*'.format(opts.host, DATA_PATH, seqid)
        command = [
            'wget',
            '-q',
            '-r',
            '-nH',
            '--cut-dirs=2',
            '-P',
            opts.subdir,
            '--ftp-user={0}'.format(opts.user),
            '--ftp-password={0}'.format(opts.password),
            url
        ]

        seqdir = '{0}_raw'.format(seqid)
        if not os.path.isdir(seqdir):
            sys.stderr.write('ERROR: No sequence directory found\n')
            continue

        try:
            os.makedirs(os.path.join(seqdir, opts.subdir))
        except OSError:
            pass

        subprocess.call(command, cwd=seqdir)
        rd = redis.StrictRedis()
        rd.lpush('stms2:sequence', seqid)


if __name__ == '__main__':
    main()
