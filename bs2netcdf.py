#!/usr/bin/env python
"""
%prog [options] cfgfile datafile ncfile

Convert an STMS2 backscatter data-file to netCDF format.
"""
import scipy.io
from datetime import datetime, timedelta
from dateutil.tz import tzstr, tzutc
import numpy as np
from netCDF4 import Dataset
from optparse import OptionParser
from collections import namedtuple
import yaml
import os


Receiver = namedtuple('Receiver', 'channel name gain')
Transmitter = namedtuple('Transmitter', 'name tvolts transducer pulse')

# Peak-to-peak voltage range of the A/D input
ADC_VPP = 20.
epoch = datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
local_tz = tzstr('CST6CDT')


def unixtime(dt):
    """
    Convert a datetime object to seconds since 1/1/1970 UTC.
    """
    td = dt - epoch
    secs = (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10.**6)/10.**6
    return secs


def datenum2dt(dnum, tz):
    """
    Convert a Matlab datenum to a datetime object.
    """
    # http://sociograph.blogspot.com/2011/04/how-to-avoid-gotcha-when-converting.html
    dt = datetime.fromordinal(int(dnum)) + timedelta(days=dnum % 1) - timedelta(days=366)
    return dt.replace(tzinfo=tz)


def write_stms2_nc(filename, mat, receivers, xmtr, **kwds):
    """
    Create an STMS2 netcdf file from the Matlab backscatter data.

    :param filename: name of output netcdf file.
    :param mat: Matlab data dictionary.
    :param receivers: list of Receiver objects.
    :param xmtr: Transmitter object.
    """
    nc = Dataset(filename, 'w', format='NETCDF3_CLASSIC')
    nc.createDimension('rindex', len(receivers))
    nc.createDimension('tau', mat['ad_data_volts'].shape[0])
    rnum = []
    gains = []
    # workaround a bug in the Matlab netcdf plotting program
    namelen = 15
    for receiver in receivers:
        namelen = max(namelen, len(receiver.name))
        rnum.append(receiver.channel)
        gains.append(receiver.gain)
    nc.createDimension('namelen', namelen)
    setattr(nc, 'transmitter', xmtr.name)
    setattr(nc, 'transducer', xmtr.transducer)
    setattr(nc, 'pulsefile', xmtr.pulse)
    for k, v in kwds.items():
        setattr(nc, k, v)
    t0 = unixtime(datenum2dt(mat['date_time'][0, 0], local_tz))
    t_secs = nc.createVariable('time', 'i4', ())
    t_secs.units = 'seconds since 1/1/1970 00:00:00+00:00'
    t_secs.long_name = 'trigger time'
    t_usecs = nc.createVariable('usecs', 'i4', ())
    t_usecs.units = 'microseconds'
    period = nc.createVariable('period', 'f4', ())
    period.units = 'seconds'
    vpb = nc.createVariable('vpb', 'f4', ())
    vpb.units = 'volts'
    vpb.long_name = 'receiver volts per bit'
    tvolts = nc.createVariable('tvolts', 'f4', ())
    tvolts.units = 'volts'
    tvolts.long_name = 'peak transmit voltage'
    rnums = nc.createVariable('rnum', 'i4', ('rindex', ))
    rnums.long_name = 'receiver a/d channel'
    rnames = nc.createVariable('rname', 'c', ('rindex', 'namelen'))
    rnames.long_name = 'STMS2 receiver name'
    gain = nc.createVariable('gain', 'i4', ('rindex', ))
    gain.units = 'dB'
    gain.long_name = 'programmable gain'
    preamp = nc.createVariable('preamp', 'i4', ('rindex', ))
    preamp.units = 'dB'
    preamp.long_name = 'fixed gain'
    rdata = nc.createVariable('rdata', 'i2', ('tau', 'rindex'))
    rdata.long_name = 'received A/D data'
    rdata.units = 'counts'
    t_secs.assignValue(int(t0))
    t_usecs.assignValue(int((t0 % 1) * 1000000))
    period.assignValue(1. / mat['actual_ad_sample_rate'][0, 0])
    scale = ADC_VPP / 65536.
    vpb.assignValue(scale)
    tvolts.assignValue(xmtr.tvolts)
    rnums[:] = rnum
    # space-pad the string variables out to maxlen
    fmt = '%%-%ds' % namelen
    rnames[:] = [fmt % r.name for r in receivers]
    preamp[:] = np.zeros(len(receivers), np.int32)
    gain[:] = gains
    # convert the A/D voltages back to counts
    rdata[:] = np.rint(mat['ad_data_volts'] / scale)


def main():
    parser = OptionParser(usage=__doc__)
    opts, args = parser.parse_args()
    if len(args) < 3:
        parser.error('Missing arguments')
    cfg = yaml.load(open(args[0], 'r'))
    mat = scipy.io.loadmat(args[1])
    dirs, mfile = os.path.split(args[1])
    mat_name = os.path.join(os.path.basename(dirs), mfile)
    receivers = []
    for r in cfg['receivers']:
        receivers.append(Receiver(name=r['name'],
                                  channel=r['channel'],
                                  gain=r['gain']))
    t = cfg['transmitter']
    xmtr = Transmitter(name=t['name'],
                       transducer=t['transducer'],
                       pulse=t['pulse'],
                       tvolts=t['tvolts'])
    write_stms2_nc(args[2], mat, receivers, xmtr,
                   mat_name=mat_name)


if __name__ == '__main__':
    main()
